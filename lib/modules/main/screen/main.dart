import 'package:ciwee_mobile/utils/media_query_util.dart';
import 'package:ciwee_mobile/widgets/button/lw_text_button.dart';
import 'package:ciwee_mobile/widgets/button/type_button.dart';
import 'package:ciwee_mobile/widgets/logo/logo.dart';
import 'package:flutter/material.dart';

class Main extends StatefulWidget {
  const Main({Key? key}) : super(key: key);

  @override
  State<Main> createState() => _MainState();
}

class _MainState extends State<Main> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.only(top: 30, left: 50, right: 50),
          child: Column(
            children: [
              SizedBox(
                height: MediaQUtil.piece(context, 0.15),
                child: const Image(
                  image: AssetImage("assets/logo-ciwee-v3.png"),
                ),
              ),
              SizedBox(
                height: MediaQUtil.piece(context, 0.25),
                child: const Logo(),
              ),
              SizedBox(
                height: MediaQUtil.piece(context, 0.55),
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 50),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      LwTextButton(
                        icon: Icons.app_registration_rounded,
                        typeButton: TypeButton.LIGHT,
                        text: 'Criar nova conta',
                        onPressed: () {
                          Navigator.of(context)
                              .pushNamed("/create_new_account");
                        },
                      ),
                      const SizedBox(height: 10),
                      LwTextButton(
                        icon: Icons.login_rounded,
                        typeButton: TypeButton.PRIMARY,
                        text: 'Acessar conta',
                        onPressed: () {
                          Navigator.of(context).pushNamed("/login");
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
