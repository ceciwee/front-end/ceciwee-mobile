import 'package:ciwee_mobile/utils/media_query_util.dart';
import 'package:ciwee_mobile/widgets/button/lw_text_button.dart';
import 'package:ciwee_mobile/widgets/button/type_button.dart';
import 'package:ciwee_mobile/widgets/button_go_back/lw_app_bar_customized.dart';
import 'package:ciwee_mobile/widgets/logo/logo.dart';
import 'package:ciwee_mobile/widgets/text_field/lw_text_field.dart';
import 'package:flutter/material.dart';

class RecoverPassword extends StatefulWidget {
  const RecoverPassword({Key? key}) : super(key: key);

  @override
  State<RecoverPassword> createState() => _RecoverPasswordState();
}

class _RecoverPasswordState extends State<RecoverPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LwAppBarCutomized(route: "/login"),
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.only(left: 50, right: 50),
          child: Column(
            children: [
              SizedBox(
                height: MediaQUtil.piece(context, 0.25),
                child: const Logo(),
              ),
              SizedBox(
                height: MediaQUtil.piece(context, 0.65),
                child: Column(
                  children: [
                    Text(
                      'Informe seu email para poder recuperar a senha',
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                    const SizedBox(height: 10),
                    LwTextField(
                      labelText: 'Email',
                      icon: Icons.person_pin_rounded,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.emailAddress,
                      onSubmitted: (value) {
                        Navigator.pushNamed(context, "/login");
                      },
                    ),
                    const SizedBox(height: 10),
                    LwTextButton(
                      icon: Icons.login_rounded,
                      typeButton: TypeButton.PRIMARY,
                      text: 'Enviar',
                      onPressed: () {
                        Navigator.of(context).pushNamed("/login");
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
