import 'package:ciwee_mobile/themes/light_theme.dart';
import 'package:ciwee_mobile/utils/media_query_util.dart';
import 'package:ciwee_mobile/widgets/button/lw_text_button.dart';
import 'package:ciwee_mobile/widgets/button/type_button.dart';
import 'package:ciwee_mobile/widgets/button_go_back/lw_app_bar_customized.dart';
import 'package:ciwee_mobile/widgets/logo/logo.dart';
import 'package:ciwee_mobile/widgets/text_field/lw_text_field.dart';
import 'package:flutter/material.dart';

class CreateNewAccount extends StatefulWidget {
  const CreateNewAccount({Key? key}) : super(key: key);

  @override
  State<CreateNewAccount> createState() => _CreateNewAccountState();
}

class _CreateNewAccountState extends State<CreateNewAccount> {
  bool _obscureTextFirst = true;
  bool _obscureTextSecond = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LwAppBarCutomized(route: "/"),
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.only(left: 50, right: 50),
          child: Column(
            children: [
              SizedBox(
                height: MediaQUtil.piece(context, 0.25),
                child: const Logo(),
              ),
              SizedBox(
                height: MediaQUtil.piece(context, 0.5),
                child: Column(
                  children: [
                    LwTextField(
                      labelText: 'Email',
                      icon: Icons.person_pin_rounded,
                      keyboardType: TextInputType.emailAddress,
                      textInputAction: TextInputAction.next,
                    ),
                    const SizedBox(height: 10),
                    LwTextField(
                      labelText: 'Apelido',
                      icon: Icons.person_pin_rounded,
                      keyboardType: TextInputType.emailAddress,
                      textInputAction: TextInputAction.next,
                    ),
                    const SizedBox(height: 10),
                    LwTextField(
                      labelText: 'Senhar',
                      obscureText: _obscureTextFirst,
                      icon: Icons.person_pin_rounded,
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            _obscureTextFirst = !_obscureTextFirst;
                          });
                        },
                        icon: _obscureTextFirst
                            ? const Icon(Icons.visibility_rounded, size: 20)
                            : const Icon(Icons.visibility_off_rounded,
                                size: 20),
                      ),
                      textInputAction: TextInputAction.next,
                    ),
                    const SizedBox(height: 10),
                    LwTextField(
                      labelText: 'Confirma Senha',
                      obscureText: _obscureTextSecond,
                      icon: Icons.person_pin_rounded,
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            _obscureTextSecond = !_obscureTextSecond;
                          });
                        },
                        icon: _obscureTextSecond
                            ? const Icon(Icons.visibility_rounded, size: 20)
                            : const Icon(Icons.visibility_off_rounded,
                                size: 20),
                      ),
                      textInputAction: TextInputAction.done,
                      onSubmitted: (value) {
                        Navigator.of(context).pushNamed("/login");
                      },
                    ),
                    const SizedBox(height: 10),
                    LwTextButton(
                      icon: Icons.login_rounded,
                      typeButton: TypeButton.PRIMARY,
                      text: 'Registrar',
                      onPressed: () {
                        Navigator.of(context).pushNamed("/login");
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
