import 'package:ciwee_mobile/themes/light_theme.dart';
import 'package:ciwee_mobile/utils/media_query_util.dart';
import 'package:ciwee_mobile/widgets/button/lw_text_button.dart';
import 'package:ciwee_mobile/widgets/button/type_button.dart';
import 'package:ciwee_mobile/widgets/button_go_back/lw_app_bar_customized.dart';
import 'package:ciwee_mobile/widgets/logo/logo.dart';
import 'package:ciwee_mobile/widgets/text_field/lw_text_field.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool _obscureText = true;

  void _onObscureText(bool obscureText) {
    setState(() {
      _obscureText = obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LwAppBarCutomized(route: "/"),
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.only(left: 50, right: 50),
          child: Column(
            children: [
              SizedBox(
                height: MediaQUtil.piece(context, 0.15),
                child: const Image(
                  image: AssetImage("assets/logo-ciwee-v3.png"),
                ),
              ),
              SizedBox(
                height: MediaQUtil.piece(context, 0.25),
                child: const Logo(),
              ),
              SizedBox(
                height: MediaQUtil.piece(context, 0.5),
                child: Column(
                  children: [
                    LwTextField(
                      labelText: 'Email ou Apelido',
                      icon: Icons.person_pin_rounded,
                      keyboardType: TextInputType.emailAddress,
                      textInputAction: TextInputAction.next,
                    ),
                    const SizedBox(height: 10),
                    LwTextField(
                      labelText: 'Senhar',
                      obscureText: _obscureText,
                      icon: Icons.person_pin_rounded,
                      suffixIcon: IconButton(
                        onPressed: () {
                          _onObscureText(!_obscureText);
                        },
                        icon: _obscureText
                            ? const Icon(Icons.visibility_rounded, size: 20)
                            : const Icon(Icons.visibility_off_rounded,
                                size: 20),
                      ),
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.visiblePassword,
                      onSubmitted: (value) {
                        Navigator.of(context).pushNamed("store_list");
                      },
                    ),
                    const SizedBox(height: 10),
                    LwTextButton(
                      icon: Icons.login_rounded,
                      typeButton: TypeButton.PRIMARY,
                      text: 'Entrar',
                      onPressed: () {
                        Navigator.of(context).pushNamed("/store_list");
                      },
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Esqueceu sua Senha?",
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.of(context)
                                .pushNamed("/recover_password");
                          },
                          child: const Text(
                            "Sim!",
                            style: TextStyle(
                              color: LightTheme.monochromatic200,
                              fontSize: 12,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Material(
                      color: LightTheme.defaultColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: TextButton(
                        style: ButtonStyle(
                          padding: MaterialStateProperty.all<EdgeInsets>(
                            const EdgeInsets.all(15),
                          ),
                        ),
                        onPressed: () {},
                        child: const Image(
                          image: AssetImage("assets/google_logo.png"),
                          height: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
