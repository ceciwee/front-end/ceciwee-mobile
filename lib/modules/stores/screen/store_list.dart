import 'package:ciwee_mobile/modules/stores/screen/search.dart';
import 'package:ciwee_mobile/modules/stores/service/store_repository.dart';
import 'package:ciwee_mobile/themes/light_theme.dart';
import 'package:ciwee_mobile/widgets/button_go_back/lw_app_bar_customized.dart';
import 'package:ciwee_mobile/widgets/item_view/item_view.dart';
import 'package:flutter/material.dart';

class StoreList extends StatefulWidget {
  const StoreList({Key? key}) : super(key: key);

  @override
  State<StoreList> createState() => _StoreListState();
}

class _StoreListState extends State<StoreList> {
  late StoreRepository storeRepository;
  final loading = ValueNotifier(true);
  late final ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _scrollController.addListener(infinitScrolling);
    storeRepository = StoreRepository();
    loadStores();
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  infinitScrolling() {
    if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent &&
        !loading.value) {
      loadStores();
    }
  }

  loadStores() async {
    loading.value = true;
    await storeRepository.getStores();
    loading.value = false;
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: LwAppBarCutomized(
        route: "/login",
        returnActivated: false,
        title: 'Lojas',
        actions: [
          IconButton(
            onPressed: _onSearch,
            icon: const Icon(
              Icons.search_rounded,
              size: 18,
            ),
          ),
          PopupMenuButton(
            offset: const Offset(0, 45),
            elevation: 0.7,
            color: LightTheme.white,
            icon: ClipRRect(
              borderRadius: BorderRadius.circular(10), // Image border
              child: SizedBox.fromSize(
                size: const Size.fromRadius(40), // Image radius
                child: const Image(
                  fit: BoxFit.cover,
                  image: AssetImage('assets/user.png'),
                ),
              ),
            ),
            itemBuilder: (context) {
              return [
                PopupMenuItem<int>(
                  value: 1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Icon(Icons.logout_rounded, size: 18),
                      Text(
                        'Sair',
                        style: Theme.of(context).textTheme.overline,
                      ),
                    ],
                  ),
                ),
              ];
            },
            onSelected: (value) {
              if (value == 0) {
                Navigator.of(context).pop();
              } else if (value == 1) {
                Navigator.of(context).pushNamed("/");
              }
            },
          ),
        ],
      ),
      body: SafeArea(
        child: AnimatedBuilder(
          animation: storeRepository,
          builder: (context, child) {
            return Stack(
              children: [
                ListView.separated(
                  controller: _scrollController,
                  itemBuilder: (context, index) {
                    final store = storeRepository.stores[index];
                    return ItemView(
                      title: store.title,
                      decription: store.description,
                      image: store.icon,
                      onPressed: () {
                        Navigator.of(context).pushNamed("/main_page");
                      },
                    );
                  },
                  separatorBuilder: (_, __) => Container(),
                  itemCount: storeRepository.stores.length,
                ),
                loadingIndicator()
              ],
            );
          },
        ),
      ),
    );
  }

  loadingIndicator() {
    return ValueListenableBuilder(
      valueListenable: loading,
      builder: (context, bool isLoading, _) {
        return isLoading
            ? Positioned(
                left: (MediaQuery.of(context).size.width / 2) - 20,
                bottom: 24,
                child: const SizedBox(
                  height: 40,
                  width: 40,
                  child: CircleAvatar(
                    child: SizedBox(
                      width: 20,
                      height: 20,
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                      ),
                    ),
                  ),
                ),
              )
            : Container();
      },
    );
  }

  _onSearch() {
    scaffoldKey.currentState?.showBottomSheet((_) {
      return const Search();
    });
  }
}
