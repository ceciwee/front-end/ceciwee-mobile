import 'package:ciwee_mobile/themes/light_theme.dart';
import 'package:ciwee_mobile/utils/media_query_util.dart';
import 'package:ciwee_mobile/widgets/button/lw_text_button.dart';
import 'package:ciwee_mobile/widgets/button/type_button.dart';
import 'package:ciwee_mobile/widgets/text_field/lw_text_field.dart';
import 'package:flutter/material.dart';

class Search extends StatefulWidget {
  const Search({Key? key}) : super(key: key);

  @override
  State<Search> createState() => _SearchState();
}

class _SearchState extends State<Search> {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: LightTheme.eighthColor,
      child: Container(
        decoration: const BoxDecoration(
          border: Border.symmetric(
            horizontal: BorderSide(
              width: 0.3,
              color: LightTheme.fifthColor,
            ),
          ),
        ),
        // color: Colors.transparent,
        height: MediaQUtil.piece(context, 0.26),
        padding: const EdgeInsets.all(15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Icon(Icons.keyboard_arrow_down_rounded),
            LwTextField(
              labelText: 'Pesquisar Loja',
              icon: Icons.person_pin_rounded,
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.done,
              onSubmitted: (value) {},
            ),
            LwTextButton(
              icon: Icons.login_rounded,
              typeButton: TypeButton.PRIMARY,
              text: 'Pesquisar',
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        ),
      ),
    );
  }
}
