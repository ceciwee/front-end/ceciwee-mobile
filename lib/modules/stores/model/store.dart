class Store {
  final String title;
  final String description;
  final String icon;
  Store({required this.title, required this.description, required this.icon});

  factory Store.fronMap(Map<String, dynamic> map) {
    return Store(
      title: map['title'] ?? '',
      description: map['description'] ?? '',
      icon: map['icon'] ?? "assets/renner.png",
    );
  }
}
