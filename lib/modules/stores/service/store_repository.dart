import 'dart:io';
import 'dart:math';

import 'package:ciwee_mobile/modules/stores/model/store.dart';
import 'package:flutter/widgets.dart';

class StoreRepository extends ChangeNotifier {
  final List<Store> _stores = [];
  late int id = 1;

  List<Store> get stores => _stores;

  getStores() async {
    for (var i = 1; i <= 10; i++) {
      String icon = '';
      Random random = Random();
      int value = random.nextInt(3);
      switch (value) {
        case 1:
          icon = 'assets/renner.png';
          break;
        case 2:
          icon = 'assets/riachuelo.png';
          break;
        case 3:
          icon = 'assets/nike.jpg';
          break;
        default:
          icon = 'assets/nike.jpg';
      }

      _stores.add(
        Store(
          title: 'title${id}',
          description: 'Note que a vulnerabilidade é no processo de geração de chaves.',
          icon: icon,
        ),
      );
      id++;
    }

    notifyListeners();
    sleep(const Duration(seconds: 1));
  }
}
