enum GlobalVariable {
  elevationZero(0.0),
  elevationLow(1.0),
  elevationMedium(2.0),
  elevationHigth(4.0),
  ;

  const GlobalVariable(this.value);
  final double value;
}
