import 'package:ciwee_mobile/modules/login/screen/create_new_account.dart';
import 'package:ciwee_mobile/modules/main/screen/main.dart';
import 'package:ciwee_mobile/modules/login/screen/recover_password.dart';
import 'package:ciwee_mobile/modules/login/screen/login.dart';
import 'package:ciwee_mobile/modules/main/screen/mainPage.dart';
import 'package:ciwee_mobile/modules/stores/screen/store_list.dart';
import 'package:flutter/material.dart';

class Routes {
  static Map<String, WidgetBuilder> getRoute() {
    return <String, WidgetBuilder>{
      '/': (context) => const Main(),
      '/login': (context) => const Login(),
      '/recover_password': (context) => const RecoverPassword(),
      '/create_new_account': (context) => const CreateNewAccount(),
      '/store_list': (context) => const StoreList(),
      '/main_page': (context) => MainPage(),
    };
  }
}
