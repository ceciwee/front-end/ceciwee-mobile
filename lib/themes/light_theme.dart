import 'package:ciwee_mobile/widgets/button/type_button.dart';
import 'package:flutter/material.dart';

class LightTheme {
  static const Color defaultColor = Color(0xFFEEEEEE);
  static const Color transparent = Colors.transparent;
  static const Color firstColor = Color(0xff00D1FE);
  static const Color secondColor = Color(0xff1d2635);
  static const Color thirtyColor = Color(0xff383838);
  static const Color fourthColor = Color(0xff797878);
  static const Color fifthColor = Color(0xffC4C2C2);
  static const Color sixthColor = Color(0xff858383);
  static const Color seventhColor = Color(0xff5E5D5D);
  static const Color eighthColor = Color.fromARGB(255, 238, 236, 233);
  static const Color ninethColor = Color.fromARGB(255, 219, 217, 215);

  // Monochromatic
  static const Color monochromatic100 = Color(0xff006880);
  static const Color monochromatic200 = Color(0xff4DDEFF);
  static const Color monochromatic300 = Color(0xff266F80);
  static const Color monochromatic400 = Color(0xff00A7CC);

  // Analogous
  static const Color analogue100 = Color(0xff0513F9);
  static const Color analogue200 = Color(0xff0449DB);
  static const Color analogue300 = Color(0xff0CE8D5);
  static const Color analogue400 = Color(0xff05F9DD);

  // Triad
  static const Color triad100 = Color(0xff1C6CA6);
  static const Color triad200 = Color(0xffF22D29);
  static const Color triad300 = Color(0xffF2F229);
  static const Color triad400 = Color(0xffA6A503);

  //Complementary
  static const Color complement100 = Color(0xff0663A6);
  static const Color complement200 = Color(0xff2BA7FF);
  static const Color complement300 = Color(0xffA66300);
  static const Color complement400 = Color(0xffF29711);

  // Split Complementary
  static const Color splitComplem100 = Color(0xffA6471C);
  static const Color splitComplem200 = Color(0xffF25F1D);
  static const Color splitComplem300 = Color(0xffA68A1C);
  static const Color splitComplem400 = Color(0xffF2C205);

  //Double Split Complementary
  static const Color doubleSplComp100 = Color(0xff1D29F2);
  static const Color doubleSplComp200 = Color(0xffF26729);
  static const Color doubleSplComp300 = Color(0xffF2C929);
  static const Color doubleSplComp400 = Color(0xff05F2D6);

  //Square
  static const Color square100 = Color(0xff299EF2);
  static const Color square200 = Color(0xffF229E7);
  static const Color square300 = Color(0xffF2A129);
  static const Color square400 = Color(0xff4EF205);

  // Compound
  static const Color compound100 = Color(0xff212ABF);
  static const Color compound200 = Color(0xff42468C);
  static const Color compound300 = Color(0xffF4994E);
  static const Color compound400 = Color(0xffBF5221);

  // Shades
  static const Color shades100 = Color(0xff0C6DB3);
  static const Color shades200 = Color(0xff084673);
  static const Color shades300 = Color(0xff129CFF);
  static const Color shades400 = Color(0xff0F85D9);

  // Tetradic
  static const Color tetradic100 = Color(0xffE111F2);
  static const Color tetradic200 = Color(0xffF27011);
  static const Color tetradic300 = Color(0xff22F211);

  // outras
  static const Color white = Color(0xFFF8F8F8);
  static const Color whiteSecodary = Color(0xFFDDDDDD);

  // Depreciado
  static const Color btPrimary = Color(0xff1193F2);
  static const Color btAcentuado = Color(0xff42468C);
  static const Color btSecondary = Color(0xff05F2D6);
  static const Color btDefault = Color(0xff05F2D6);

  static const Color titleTextColor = Color(0xff1d2635);
  static const Color subTitleTextColor = Color(0xff797878);

  static const Color skyBlue = Color(0xff2890c8);
  static const Color lightBlue = Color(0xffDE3763);

  static const Color orangePrimary = Color(0xffE65829);
  static const Color orangeSecondary = Color(0xffec7254);
  static const Color red = Color.fromARGB(255, 255, 38, 0);

  static const Color lightGrey = Color(0xffE1E2E4);
  static const Color grey = Color(0xffA1A3A6);
  static const Color darkgrey = Color(0xff747F8F);

  static const Color iconColor = Color(0xffa8a09b);
  static const Color yellowColor = Color(0xfffbba01);

  static const Color black = Color(0xff20262C);
  static const Color lightblack = Color(0xff5F5F60);

  static LinearGradient linearGradient(TypeButton typeButton) {
    switch (typeButton) {
      case TypeButton.LIGHT:
        return const LinearGradient(colors: [
          LightTheme.ninethColor,
          LightTheme.ninethColor,
        ]);
      default:
        return const LinearGradient(colors: [
          LightTheme.firstColor,
          LightTheme.analogue300,
        ]);
    }
  }

  static Color color(TypeButton typeButton) {
    switch (typeButton) {
      case TypeButton.LIGHT:
        return LightTheme.sixthColor;
      default:
        return LightTheme.defaultColor;
    }
  }
}
