import 'package:flutter/widgets.dart';

class MediaQUtil {
  static double fullHeight({required BuildContext context}) {
    return MediaQuery.of(context).size.height;
  }

  static double fullWidth({required BuildContext context}) {
    return MediaQuery.of(context).size.width;
  }

  static double grossSizeTop(BuildContext context, {double? height}) {
    if (height == null) {
      return fullHeight(context: context) - MediaQuery.of(context).padding.top;
    }
    return MediaQuery.of(context).size.height -
        height -
        MediaQuery.of(context).padding.top;
  }

  static double grossSizeBottom(BuildContext context, {double? height}) {
    if (height == null) {
      return fullHeight(context: context) - MediaQuery.of(context).padding.top;
    }
    return fullHeight(context: context) -
        height -
        MediaQuery.of(context).padding.bottom;
  }

  static double piece(BuildContext context, double percent) {
    return grossSizeTop(context) * percent;
  }
}
