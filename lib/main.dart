import 'package:ciwee_mobile/config/routes/routes.dart';
import 'package:ciwee_mobile/themes/light_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

import 'themes/theme_config.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return MaterialApp(
      title: 'Ciwee',
      theme: ThemeConfig.lightTheme.copyWith(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: LightTheme.eighthColor,
        textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(
            textStyle: GoogleFonts.montserratAlternates(fontSize: 12),
          ),
        ),
        inputDecorationTheme: InputDecorationTheme(
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            borderSide: BorderSide(color: LightTheme.analogue300),
          ),
          iconColor: LightTheme.sixthColor,
          contentPadding: const EdgeInsets.only(bottom: 0, top: 0, left: 10),
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10),
          ),
          filled: true,
          fillColor: LightTheme.white,
          labelStyle: GoogleFonts.montserratAlternates(
            color: LightTheme.sixthColor,
            fontSize: 12,
          ),
        ),
        textTheme: GoogleFonts.latoTextTheme(textTheme).copyWith(
            subtitle2: GoogleFonts.montserratAlternates(
              color: LightTheme.sixthColor,
            ),
            headline4: GoogleFonts.montserratAlternates(
              color: LightTheme.sixthColor,
            ),
            headline5: GoogleFonts.montserratAlternates(
              color: LightTheme.sixthColor,
            ),
            bodyText2: GoogleFonts.montserratAlternates(
              color: LightTheme.sixthColor,
            ),
            overline: GoogleFonts.montserratAlternates(
              color: LightTheme.sixthColor,
            )),
      ),
      debugShowCheckedModeBanner: false,
      routes: Routes.getRoute(),
      initialRoute: "/",
    );
  }
}
