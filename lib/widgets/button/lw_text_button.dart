import 'package:ciwee_mobile/config/global_variable/global_variable.dart';
import 'package:ciwee_mobile/themes/light_theme.dart';
import 'package:ciwee_mobile/utils/media_query_util.dart';
import 'package:ciwee_mobile/widgets/button/type_button.dart';
import 'package:flutter/material.dart';

class LwTextButton extends StatelessWidget {
  LwTextButton({
    Key? key,
    required this.typeButton,
    required this.onPressed,
    required this.icon,
    this.text,
    this.width,
    this.height,
    this.elevation,
  }) : super(key: key);

  String? text;
  final TypeButton typeButton;
  final void Function() onPressed;
  final IconData icon;
  double? elevation;
  double? width;
  double? height;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: elevation ?? GlobalVariable.elevationZero.value,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: SizedBox(
        height:height ?? MediaQUtil.fullHeight(context: context) * 0.07,
        width: width ?? MediaQUtil.fullWidth(context: context),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            gradient: LightTheme.linearGradient(typeButton),
          ),
          child: TextButton(
            onPressed: onPressed,
            child: _getButton(),
          ),
        ),
      ),
    );
  }

  Widget _getButton() {
    if (text == null) {
      return Center(
          child: Icon(icon, color: LightTheme.color(typeButton), size: 20));
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          text ?? '',
          style: TextStyle(
            color: LightTheme.color(typeButton),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 10),
          child: Icon(icon, color: LightTheme.color(typeButton), size: 20),
        )
      ],
    );
  }
}
