import 'package:ciwee_mobile/themes/light_theme.dart';
import 'package:ciwee_mobile/utils/media_query_util.dart';
import 'package:ciwee_mobile/widgets/button/lw_text_button.dart';
import 'package:ciwee_mobile/widgets/button/type_button.dart';
import 'package:flutter/material.dart';

class ItemView extends StatefulWidget {
  ItemView(
      {Key? key,
      required this.title,
      required this.decription,
      required this.image,
      required this.onPressed,
      this.buttonWidth})
      : super(key: key);
  String title;
  double? buttonWidth;
  String decription;
  String image;
  final void Function() onPressed;

  @override
  State<ItemView> createState() => _ItemViewState();
}

class _ItemViewState extends State<ItemView> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Material(
        color: LightTheme.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
          ),
          height: MediaQUtil.fullHeight(context: context) * 0.12,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(10), // Image border
                child: SizedBox.fromSize(
                  size: const Size.fromRadius(30), // Image radius
                  child: Image(
                    fit: BoxFit.cover,
                    image: AssetImage(widget.image),
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.title,
                    style: Theme.of(context).textTheme.subtitle2?.merge(
                          const TextStyle(
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                  ),
                  const SizedBox(height: 10),
                  RichText(
                    text: WidgetSpan(
                      child: ConstrainedBox(
                        constraints: const BoxConstraints(maxWidth: 170),
                        child: Text(
                          widget.decription,
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2
                              ?.merge(
                                const TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w100,
                                ),
                              ),
                          textAlign: TextAlign.justify,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              LwTextButton(
                width: 60,
                height: 60,
                typeButton: TypeButton.PRIMARY,
                onPressed: widget.onPressed,
                icon: Icons.chevron_right_rounded,
              )
            ],
          ),
        ),
      ),
    );
  }
}
