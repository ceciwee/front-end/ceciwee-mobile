import 'package:ciwee_mobile/themes/light_theme.dart';
import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  const Logo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "Ciwee Stores",
          style: Theme.of(context)
              .textTheme
              .headline4
              ?.copyWith(color: LightTheme.firstColor),
        ),
        Text(
          "O futuro chegou",
          style: Theme.of(context).textTheme.subtitle2,
        ),
      ],
    );
  }
}
