import 'package:ciwee_mobile/config/global_variable/global_variable.dart';
import 'package:ciwee_mobile/utils/media_query_util.dart';
import 'package:flutter/material.dart';

class LwTextField extends StatelessWidget {
  LwTextField({
    Key? key,
    required this.labelText,
    required this.icon,
    this.onSubmitted,
    this.textInputAction,
    this.keyboardType,
    this.suffixIcon,
    this.obscureText,
  }) : super(key: key);

  final String labelText;
  final IconData icon;
  Null Function(dynamic value)? onSubmitted;
  TextInputType? keyboardType = TextInputType.name;
  TextInputAction? textInputAction = TextInputAction.next;
  IconButton? suffixIcon;
  bool? obscureText;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: GlobalVariable.elevationZero.value,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: SizedBox(
        height: MediaQUtil.fullHeight(context: context) * 0.07,
        child: TextField(
          obscureText: obscureText ?? false,
          textInputAction: textInputAction,
          onSubmitted: onSubmitted,
          keyboardType: keyboardType,
          decoration: InputDecoration(
            labelText: labelText,
            suffixIcon: suffixIcon ?? Icon(icon, size: 20),
          ),
        ),
      ),
    );
  }
}
