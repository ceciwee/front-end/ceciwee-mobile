import 'package:ciwee_mobile/config/global_variable/global_variable.dart';
import 'package:ciwee_mobile/themes/light_theme.dart';
import 'package:flutter/material.dart';

class LwAppBarCutomized extends StatefulWidget implements PreferredSizeWidget {
  LwAppBarCutomized(
      {Key? key, this.route, this.title, this.actions, this.returnActivated})
      : super(key: key);

  String? route;
  String? title;
  bool? returnActivated = true;
  List<Widget>? actions;

  @override
  State<LwAppBarCutomized> createState() => _LwAppBarCutomizedState();

  @override
  Size get preferredSize => const Size.fromHeight(50);
}

class _LwAppBarCutomizedState extends State<LwAppBarCutomized> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: widget.returnActivated ?? true,
      centerTitle: true,
      title: Text(
        widget.title ?? '',
        style: Theme.of(context).textTheme.headline5?.copyWith(fontSize: 18),
      ),
      actions: widget.actions,
      leading: widget.returnActivated ?? true
          ? IconButton(
              onPressed: () {
                if (widget.route == null) {
                  Navigator.of(context).pop(context);
                } else {
                  Navigator.of(context).pushNamed(widget.route ?? "/");
                }
              },
              icon: const Icon(
                Icons.arrow_back_ios,
                size: 16,
              ),
            )
          : null,
      iconTheme: const IconThemeData(
        color: LightTheme.fourthColor,
      ),
      backgroundColor: Colors.transparent,
      elevation: GlobalVariable.elevationZero.value,
    );
  }
}
